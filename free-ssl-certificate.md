Register domain(s)
=

1. Start from **domain key** and **Certificate signing request** (CSR) on [CSR generator page](https://zerossl.com/free-ssl/#csr)
2. Ex. if you need a certificate to run https://mariposa.com, enter ```mariposa.com``` or ```mariposa.com *.mariposa.com``` for subdomains into _Domains_ input field
3. Leave do

Renew
====

1. On the page paste:
    1. Your **Let's Encrypt certificate** in the left window
    2. Your **CSR cerftificate** in the right one
2. Click next
3. On the next page you receive a certificate file to be used with your domain certificate

Installation
=

Please note that some hosting pages may require further actions to make the certificate running. like

* Removing current (expired) certificate)
* Splitting the received sertificate into "Certificate" and "Intermediary"
* Initializing the new certificatcate for every domain and subdomain

In the most complicated case, a successful installation will require you to provide the 3 files (strings)

1. **Domain certificate**
2. **Intermediate chain** or **CA Bundle**
3. **Private key** or **Domain key**

In some case 1. and 2. above may be required as one file (as the wizard generates). If not, just split the string into two files. Plain text files (_*.txt_) should be accepted.