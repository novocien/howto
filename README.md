Access contents of a private repository with API
====

Let's assume my repository name is `game`, branch is `beta`, and the file `characters.txt` in the root folder. How do I access `/repositories/novocien/game/characters.txt` of `beta`?

If your repository is public it is enough you use `src` *endpoint* of the API.

Here it is described:  
[/2.0/repositories/{username}/{repo_slug}/src/{node}/{path}](https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/src/%7Bnode%7D/%7Bpath%7D)

   If your repository is private, you need to authenticate to bitbucket cloud. The simplest way to do it is so-called *basic authentication*

Let's assume my password is `abrakadabra` and my username `novocien`:  

`$ curl -u novocien:abrakadabra "https://api.bitbucket.org/2.0/repositories/novocien/game/src/eefd5ef/characters.txt"`  
or  
`$ curl -u novocien:abrakadabra --raw -s "https://api.bitbucket.org/2.0/repositories/novocien/game/src/eefd5ef/characters.txt"`  
if you need it.

`$ curl -u %7B1ed948e0-34de-44e1-8ef1-c9dbec02c81f%7D:abrakadabra --raw -s "https://api.bitbucket.org/2.0/repositories/novocien/game/src/eefd5ef/characters.txt"`  
with user UUID should also work.

That's all but one thing. What is *node*? How do I receive it?
----
*Node* is a reference representing a point in the history of your repository in git or mercurial. It is formatted as 40-digits hexadecimal number. It allows to reference to a **"version"** or **"revision"** of your repository or its components. You can experience it (or double check or test your code against) clicking the *Open raw* command next to the *Edit* button in your source-view window of files in bitbucket repository.

It is not the best documented aspect of the API, but I managed to find current(the most recent) node mentioned in reponse to the `refs` endpoint of the API:  
[/2.0/repositories/{username}/{repo_slug}/refs/branches/{name}](https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Busername%7D/%7Brepo_slug%7D/refs/branches/%7Bname%7D)

`$ curl -u novocien:abrakadabra -s "https://api.bitbucket.org/2.0/repositories/novocien/game/refs/branches/beta"`  

From this response you can obtain the node via javascript, PHP (look below) or any programming language you use. It is likely to change with every following commit, so it is indispensable to require it simultaneously with every request to the API.  
The branch hash should be in `heads[0]["hash']` or in `target["hash"]`. [View example response containing the branch hash of this very file](https://api.bitbucket.org/2.0/repositories/novocien/howto/refs/branches/master),  
[and an error response to a faulty API call](https://api.bitbucket.org/2.0/repositories/novocien/howto/refs/branches/default). Then call `refs` for the branch of your file and checkout the response. The `hash` contained there is ready to be put as a `{node}` to the `src` API call described above.

PHP Example
----

I needed my example in PHP, so here's the code I use, but without error defence against server downtimes or faulty internet connections:



```php
<?php
   const B="beta";//branch name
   const SLUG="character.txt";
   const REPO_PATH="https://api.bitbucket.org/2.0/repositories/novocien/game/";

function return_repo_content(){

   function getjson($url){
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_USERPWD, "{1ed948e0-34de-44e1-8ef1-c9dbec02c81f}:abrakadabra");
      $string=curl_exec($ch);
      curl_close($ch);
      return $string;
   };
   
   function gethash(){
	$json=json_decode(getjson(REPO_PATH."refs/branches/".B));
	$h=false;
	if ($json->type!="error"){//i.e."named_branch" or "commit"
		$h=$json->heads[0]->hash;//or replace with $h=$json->target->hash
	   };
	return $h;
   };

	$h=gethash();
	if ($h==false){
		return "probably no such branch or whatever error";
	};
	$content=getjson(REPO_PATH."src/".$h."/".SLUG;);
	$object=json_decode($content);
	if (gettype($object)=="object" && $object->type=="error"){//json containg an error
		return $object->error->message;
	}else{
		return $content;
	};

};

echo var_dump(return_repo_content());
?>
```

You can achieve the same result with the [FetchAPI][1] of javascript, but in case of a private repository you will be discouraged by many (and myself) to reveal your password in client-side code. Even though your repository is public, it is never an option of choice. Maybe on a page on your local computer or in your company secured network.

[1]: https://stackoverflow.com/questions/43842793/basic-authentication-with-fetch
